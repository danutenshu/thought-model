module Exceptions
  class RelationNotFoundError < StandardError; end
end
