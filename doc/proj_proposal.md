# Project Proposal - thoughtmodel


## Ideas
Originally needed a way to gather thoughts and see all the thoughts as bubbles on a screen, and try to piece thoughts together like a puzzle.

But the root idea of this model can be used for gathering ideas for a documentation, research paper, or argumentative-type paper.

## Database Choice

Not only because this is a proof of concept idea, but the fundamental.