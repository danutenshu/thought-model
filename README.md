# thought-model
A class that defines a usable model for thoughts for use in software projects.

## Use cases
This class just needed to be defined for me to be at ease cause of two original project ideas.

* I have random thoughts, and because I have goldfish memory, if I have an idea but can't seem to find any reason why I thought of it or any correlation to anything else, I'd write it down.
* Documentation or writing thorough documentation sufficient for anyone with little understanding to whatever I"m writing about, I can use ideaas as an object to post to a document and possibly update on a basis. 

## Immediate Assumptions
An idea has a description at its most raw form. Subjectively, synonyms or related keywords--*tags*--are important.

While objectively, an idea has relation, 

### Other minor attributes
An idea can...
- be *related* to other ideas. 
This is great for finding cause/effect to reach a conclusion or piecing together ideas to create a product.
- be *version-tracked*. You can log an ideas history--when the idea was created, when idea details change, when the idea is rejected.
This would go great in tracking work progress, or leads towards impactful events.
- carry a certain *weight*
Related ideas, with enough weight, can pass on their tags (or identifying keywords) onto another idea.

## Project Impact
Google's algorithm could very well be implemented using this idea at a incompetent form. It works well thinking of queries with weights against the history of percentage something was queried.